# cleaning workspace
rm(list = ls())

# load libaries
library(lmtest)
library(MSBVAR)

############################

# Functions
###########

select.lags<-function(x,y,max.lag=8) {
  y<-as.numeric(y)
  y.lag<-embed(y,max.lag+1)[,-1,drop=FALSE]
  x.lag<-embed(x,max.lag+1)[,-1,drop=FALSE]
  
  t<-tail(seq_along(y),nrow(y.lag))
  
  ms=lapply(1:max.lag,function(i) lm(y[t]~y.lag[,1:i]+x.lag[,1:i]))
  
  pvals<-mapply(function(i) anova(ms[[i]],ms[[i-1]])[2,"Pr(>F)"],max.lag:2)
  ind<-which(pvals<0.05)[1]
  ftest<-ifelse(is.na(ind),1,max.lag-ind+1)
  
  aic<-as.numeric(lapply(ms,AIC))
  bic<-as.numeric(lapply(ms,BIC))
  structure(list(ic=cbind(aic=aic,bic=bic),pvals=pvals,
                 selection=list(aic=which.min(aic),bic=which.min(bic),ftest=ftest)))
}

# Data
######

# read data in
data <- read.csv2(file="C:/Users/marcus/Desktop/PhD_Thesis/DISS_PropertyCycleModel/VariableSelectionInput/2 Testing with new data sample/StationaryDataSet.csv", head=TRUE, sep=";", dec=",")
datSel <- data[3:29]

# convert to ts object
d.cpi<-ts(datSel$cpi)
d.wages<-ts(datSel$stat.lohn)

# select lag
s<-select.lags(d.wages,d.cpi,8)
t(s$selection)

# Determining lag
#################

# for (i in length(datSel) ) {
#   for (y in length(datSel) ) {
#       d1<-ts(datSel[i])
#       d2<-ts(datSel[y])
#       lag <- select.lags(d1,d2,8)
#     
#   } 
# }
# 
# plot.ts(s$ic)


lag.aic.store = matrix(NA, 4, 4)
for (i in 1:length(datSel) ) {
  for (y in 1:length(datSel) ) {
    d1<-ts(datSel[,i])
    d2<-ts(datSel[,y])
    lag <- select.lags(d1,d2,5)
    lag.aic.store[i,y] = lag$selection$aic
  } 
}

# http://stackoverflow.com/questions/31370221/iterating-over-function-and-get-each-result-value?noredirect=1#comment50722705_31370221
# Granger Test
##############

granger <- granger.test(datSel, 6)


## Interpretation
# Estimates all possible bivariate Granger causality tests for m variables. Bivariate Granger causality
# tests for two variables X and Y evaluate whether the past values of X are useful for predicting Y
# once Y’s history has been modeled. The null hypothesis is that the past p values of X do not help in
# predicting the value of Y.
# The test is implemented by regressing Y on p past values of Y and p past values of X. An F-test is
# then used to determine whether the coefficients of the past values of X are jointly zero.
# This produces a matrix with m*(m-1) rows that are all of the possible bivariate Granger causal
# relations. The results include F-statistics and p-values for each test. Tests are estimated using single
# equation OLS models. -> taken from http://cran.r-project.org/web/packages/MSBVAR/MSBVAR.pdf
# TODO: check p-value with 0.05 confidence level

granger < 0.05

# lmtest::grangertest(datSel$oenb_dependent,datSel$gdp,4)


## TODO: checkout stackoverflow question!!!