
# cleaning workspace
rm(list = ls())


# read data in
data <- read.csv2(file="C:/Users/marcus/Desktop/PhD_Thesis/DISS_PropertyCycleModel/data.csv", head=TRUE, sep=";", dec=",")
# plotting data set
dataSelection <- data[3:27]
dput(dataSelection)

names <- LETTERS[1:26]
for(i in 3:27){

  
  mypath <- file.path("C:","Users","marcus","Desktop","PhD_Thesis","DISS_PropertyCycleModel","PlotOutput",paste("myplot_", names[i], ".png", sep = ""))
  
  png(file=mypath)
  mytitle = paste("my title is", names[i])
  plot(dataSelection[,i], main = colnames(dataSelection[i]), type = "l")
  dev.off()
}

####################################
path <- "C:/Users/marcus/Desktop/PhD_Thesis/DISS_PropertyCycleModel/VariableSelectionInput/2 Testing with new data sample/data_FINAL_stationarityAdjusted_variableSelected.csv"

# read data in
data <- read.csv2(file = path, head = TRUE, sep = ";", dec = ",")
dataSelection <- data[3:9]

##############################
# dataSelection$oenb_dependent
mypath <- file.path("C:","Users","marcus","Desktop","PhD_Thesis","DISS_PropertyCycleModel","PlotOutput",paste("myplot_", "dataSelection$oenb_dependent", ".png", sep = ""))

png(file=mypath)
plot(dataSelection$oenb_dependent, main = colnames("Rental Index"), type = "l", xlab='Time', ylab='Rental Index')
axis(1, at=dataSelection$oenb_dependent, labels=data$Date_Quandl)
dev.off()
